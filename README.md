### CS157A_Team11 (Inhee Park | Tracy Ho)

## GNT-Market 
Grocery Nutrient Tracker
<blockquote>
Main theme of this web app is to help users to get a convenient and healthy selection for the grocery list. Users can generate their grocery list by selecting a dish, which internally converts a dish to ingredient food items and/or by adding food items directly. User customized dietary restrictions and preferences are taken into consideration when generating a grocery list. 
</blockquote>

<img src="/img/updated_ERD.png" width="650">

<img src="/img/updated_schema.png"  width="550">

<img src="/img/3tier.png"  width="650">

### WebApp Screenshots
<img src="/img/welcome.png"  width="700">
<img src="/img/welcome2.png"  width="700">
<img src="/img/food.png"  width="700">
<img src="/img/result.png"  width="700">
<img src="/img/dish.png"  width="700">
<img src="/img/hamburger.png"  width="700">
<img src="/img/hamburger2.png"  width="700">
<img src="/img/check.png"  width="700">
<img src="/img/final.png"  width="700">

					
		
				
				
